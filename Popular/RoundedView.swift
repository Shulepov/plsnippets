//
//  RoundedView.swift
//  Football
//
//  Created by Mikhail Shulepov on 29/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

class RoundedView: ShapeView {
    var insets: UIEdgeInsets = UIEdgeInsets.zero {
        didSet {
            self.refresh()
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return self.shapeLayer().lineWidth
        }
        set {
            self.shapeLayer().lineWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor {
        get {
            return UIColor(cgColor: self.shapeLayer().strokeColor!)
        }
        set {
            self.shapeLayer().strokeColor = newValue.cgColor
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 10 {
        didSet {
            if self.cornerRadius != oldValue {
                self.refresh()
            }
        }
    }
    
    @IBInspectable var roundTop: Bool = false {
        didSet {
            if self.roundTop {
                 self.roundedCorners = self.roundedCorners.union([.topLeft,.topRight])
            } else {
                self.roundedCorners = self.roundedCorners.subtracting([.topLeft, .topRight])
            }
        }
    }
    
    @IBInspectable var roundBottom: Bool = false {
        didSet {
            if self.roundBottom {
                self.roundedCorners = self.roundedCorners.union([.bottomLeft, .bottomRight])
            } else {
                self.roundedCorners = self.roundedCorners.subtracting([.bottomLeft, .bottomRight])
            }
        }
    }
    
    var roundedCorners: UIRectCorner = [] {
        didSet {
            if self.roundedCorners != oldValue {
                self.refresh()
            }
        }
    }
    
    override var bounds: CGRect {
        didSet {
            if self.bounds != oldValue {
                self.refresh()
            }
        }
    }
    
    override var frame: CGRect {
        didSet {
            if self.frame != oldValue {
                self.refresh()
            }
        }
    }
    
    override var backgroundColor: UIColor? {
        get {
            return UIColor(cgColor: self.shapeLayer().fillColor!)
        }
        set {
            self.shapeLayer().fillColor = newValue?.cgColor
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    fileprivate func refresh() {
        var rect = self.bounds
        let borderInset = self.borderWidth / 2
        rect.origin.y += self.insets.top + borderInset; rect.size.height -= self.insets.top + borderInset
        rect.size.height -= self.insets.bottom + borderInset
        rect.origin.x += self.insets.left + borderInset; rect.size.width -= self.insets.left + borderInset
        rect.size.width -= self.insets.right + borderInset
        
        let radius = CGSize(width: self.cornerRadius, height: self.cornerRadius)
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: self.roundedCorners, cornerRadii: radius)
        self.shapeLayer().path = path.cgPath
    }
}
