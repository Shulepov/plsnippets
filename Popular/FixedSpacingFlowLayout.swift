//
//  GridLayoutBehavior.swift
//  Football
//
//  Created by Mikhail Shulepov on 29/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

// Resizes items to fill screen remaining spacing as specified |-item-item-item|

class FixedSpacingFlowLayout: UICollectionViewFlowLayout {
    @IBInspectable var columnsCount: Int = 2
    @IBInspectable var spacing: CGFloat = 1
    @IBInspectable var resizeLastItems: Bool = true
    @IBInspectable var centerLastItems: Bool = false
    
    fileprivate var sectionSizes = [Int]()
    
    override func prepare() {
        let frame = self.collectionView!.bounds
        let itemWidth = (frame.size.width - self.spacing * (CGFloat(columnsCount) - 1)
            - self.sectionInset.left - self.sectionInset.right ) / CGFloat(self.columnsCount)
        let oldItemSize = self.itemSize
        self.itemSize = CGSize(width: itemWidth, height: oldItemSize.height)
        self.minimumInteritemSpacing = self.spacing
        self.minimumLineSpacing = self.spacing
        
        if let dataSource = self.collectionView?.dataSource {
            let sectionsCount = dataSource.numberOfSections?(in: self.collectionView!) ?? 1
            self.sectionSizes.removeAll(keepingCapacity: true)
            for section in 0..<sectionsCount {
                let itemsNumber = dataSource.collectionView(self.collectionView!, numberOfItemsInSection: section)
                self.sectionSizes.append(itemsNumber)
            }
        }
        
        super.prepare()
    }
    
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let attrs = super.layoutAttributesForElements(in: rect)
        let viewFrame = self.collectionView!.frame
        let insets = self.sectionInset
        let totalWidth = viewFrame.width - insets.left - insets.right
        
        if let attrs = attrs {
            for attr in attrs {
                if attr.representedElementCategory != .cell {
                    continue
                }
                let section = attr.indexPath.section
                let item = attr.indexPath.item
                let countInSection = self.sectionSizes[section]
                let countInLastRow = countInSection % self.columnsCount
                if countInLastRow != 0 {
                    //if last items
                    if item >= countInSection - countInLastRow {
                        let itemColumn = item % countInLastRow
                        if self.resizeLastItems {
                            let columnWidth = (totalWidth - self.spacing * (CGFloat(countInLastRow) - 1)) /
                                CGFloat(countInLastRow)
                        
                            var frame = attr.frame
                            frame.size.width = columnWidth
                            frame.origin.x = insets.left + (columnWidth + self.spacing) * CGFloat(itemColumn)
                            attr.frame = frame
                            
                        } else if (self.centerLastItems) {
                            let totalRowWidth = CGFloat(countInLastRow - 1) * self.spacing + CGFloat(countInLastRow) * self.itemSize.width
                            let offset = (totalWidth - totalRowWidth) / 2
                            
                            var frame = attr.frame
                            frame.origin.x += offset
                            attr.frame = frame
                        }
                    }
                }
            }
        }
        return attrs
    }
}
