//
//  InfixInitializer.swift
//  davinci
//
//  Created by Mihail Shulepov on 19/11/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import Foundation

infix operator <|

func <|<T>(decl:  @autoclosure () -> T, f: (T) -> () ) -> T {
    let obj = decl()
    f(obj)
    return obj
}
