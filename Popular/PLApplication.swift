//
//  PLApplication.swift
//  davinci
//
//  Created by Mihail Shulepov on 25/12/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit

class PLApplication: UIApplication {
    var affiliateToken = "10lJDF"
    
    override func openURL(_ url: URL) -> Bool {
        var replacementURL: URL?
        let strURL = url.absoluteString
        if strURL.range(of: "itunes", options: .caseInsensitive) != nil {
            var newUrlStr: String?
            if strURL.range(of: "at=", options: []) != nil {
                newUrlStr = replaceUrlParameter(strURL, parameter: "at", replacement: affiliateToken)
            } else if let _ = url.query {
                newUrlStr = strURL + "&at=\(affiliateToken)"
            } else {
                newUrlStr = strURL + "?at=\(affiliateToken)"
            }
            
            if var newUrlStr = newUrlStr {
                if newUrlStr.range(of: "ct=", options: []) != nil {
                    newUrlStr = replaceUrlParameter(strURL, parameter: "ct", replacement: "ios_replacement") ?? newUrlStr
                } else {
                    newUrlStr += "&ct=ios_replacement"
                }
                
                replacementURL = URL(string: newUrlStr)
            }
        }
        
        if let replacement = replacementURL {
            return super.openURL(replacement)
        } else {
            return super.openURL(url)
        }
    }
    
    private func replaceUrlParameter(_ url: String, parameter: String, replacement: String) -> String? {
        do {
            let regex = try NSRegularExpression(pattern: "\(parameter)=(\\w+)(?:&|$)", options: [])
            let range = NSMakeRange(0, url.utf16.count)
            if let match = regex.firstMatch(in: url, options: [], range: range) {
                let idRange = match.rangeAt(1)
                return (url as NSString).replacingCharacters(in: idRange, with: replacement)
            }
        } catch let error as NSError {
            NSLog("Regex error: %@", error.description)
            return nil
        }
        
        
            //} else if let error = error {
        //    NSLog("Regex error: %@", error.description)
        //    return nil
        //}
        return nil
    }
}
