//
//  Section.swift
//  Football
//
//  Created by Mikhail Shulepov on 09/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation

open class Section<U, T> {
    open let header: U
    open var items: [T]
    
    public init(header: U, items: [T]) {
        self.header = header
        self.items = items
    }
    
    public init(title: U) {
        self.header = title
        self.items = [T]()
    }
    
    open var numberOfItems: Int {
        return self.items.count
    }
    
    open func positionForItem<I: Equatable>(_ item: I) -> Int? {
        for (idx, existedItem) in self.items.enumerated()  {
            if let equatableItem = existedItem as? I {
                if item == equatableItem {
                    return idx
                }
            }
        }
        
        return nil
    }
    
    open func positionForItem(_ item: T, comparator: (T, T) -> Bool) -> Int? {
        for (idx, existedItem) in self.items.enumerated()  {
            if comparator(item, existedItem) {
                    return idx
            }
        }
        return nil
    }
}

open class Sections<U, T> {
    open var sections: [Section<U, T>]
    
    public init(_ sections: [Section<U, T>]) {
        self.sections = sections
    }
    
    public init() {
        self.sections = []
    }
    
    open func numberOfSections() -> Int {
        return self.sections.count
    }
    
    open func numberOfItemsInSection(_ section: Int) -> Int {
        return self.sections[section].numberOfItems
    }
    
    open func headerForSection(_ section: Int) -> U {
        return self.sections[section].header
    }
    
    open func itemForIndexPath(_ indexPath: IndexPath) -> T {
        return self.sections[indexPath[0]].items[indexPath[1]]
    }
    
    open subscript(index: Int) -> Section<U, T> {
        return self.sections[index]
    }
    
    open subscript(index: IndexPath) -> T {
        return self.itemForIndexPath(index)
    }
    
    open func indexPathForItem<I: Equatable>(_ item: I) -> IndexPath? {
        for (idx, section) in self.sections.enumerated() {
            if let itemPosition = section.positionForItem(item) {
                return IndexPath(item: itemPosition, section: idx)
            }
        }
        return nil
    }
    
    open func indexPathForItem(_ item: T, comparator: (T, T) -> Bool) -> IndexPath? {
        for (idx, section) in self.sections.enumerated() {
            if let itemPosition = section.positionForItem(item, comparator: comparator) {
                return IndexPath(item: itemPosition, section: idx)
            }
        }
        return nil
    }
}

