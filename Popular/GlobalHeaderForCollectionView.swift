//
//  GlobalHeaderForCollectionView.swift
//  davinci
//
//  Created by Mihail Shulepov on 28/11/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit

class GlobalHeaderForCollectionView: NSObject {
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            setup()
        }
    }
    @IBInspectable var headerNib: String!
    @IBInspectable var explicitHeight: CGFloat?
    
    func setup() {
        let nibViews = Bundle.main.loadNibNamed(self.headerNib, owner: self, options: nil)
        if let headerView = nibViews?.first as? UIView {
            var frame = headerView.frame
            if let height = self.explicitHeight {
                frame.size.height = height
            }
            frame.origin.y = -frame.size.height
            frame.size.width = self.collectionView.frame.size.width
            headerView.frame = frame
            headerView.autoresizingMask = .flexibleWidth
            self.collectionView.addSubview(headerView)
            self.collectionView!.contentInset = UIEdgeInsetsMake(frame.size.height, 0, 0, 0)
        }
    }
}
