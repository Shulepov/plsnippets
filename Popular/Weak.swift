//
//  Weak.swift
//  davinci
//
//  Created by Mihail Shulepov on 11/10/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import Foundation

/// Purpose: create weak objects for which we can't specify 'weak' attribute
/// For example, to create array of weak objects
open class Weak<T: AnyObject> {
    weak var value : T?
    init (value: T) {
        self.value = value
    }
}
