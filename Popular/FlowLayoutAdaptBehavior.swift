//
//  FlowLayoutAdaptBehavior.swift
//  davinci
//
//  Created by Mihail Shulepov on 18/11/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit

/// Slightly fine tunes item sizes for different screens
class FlowLayoutAdaptBehavior: NSObject {
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            configure()
        }
    }
    
    @IBInspectable var minColumnsCount: Int = 4
    @IBInspectable var maxItemWidth: CGFloat = 120
    //header height = (aspect * screenWidth)
    @IBInspectable var headerToScreenAspect: CGFloat = 0.13
    
    override func awakeFromNib() {
        
    }
    
    func configure() {
        let flowLayout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let initialItemSize = flowLayout.itemSize
        let aspect = initialItemSize.height / initialItemSize.width
        let screenSize = UIScreen.main.bounds.size
        
        let insets = flowLayout.sectionInset.left + flowLayout.sectionInset.right
        let minSide = min(screenSize.width, screenSize.height)
        let columnsCount: CGFloat = CGFloat(self.minColumnsCount)
        let preferredSize = (minSide - insets - (columnsCount - 1) * flowLayout.minimumInteritemSpacing) / columnsCount
        let itemSize: CGFloat = min(maxItemWidth, preferredSize)
        flowLayout.itemSize = CGSize(width: itemSize, height: itemSize * aspect)
        
        var headerSize = flowLayout.headerReferenceSize
        headerSize.height = min(self.headerToScreenAspect * minSide, headerSize.height)
        flowLayout.headerReferenceSize = headerSize
    }
}
