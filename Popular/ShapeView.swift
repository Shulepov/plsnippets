//
//  ShapeView.swift
//  Football
//
//  Created by Mikhail Shulepov on 16/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit
import QuartzCore

open class ShapeView: UIView {
    override open class var layerClass : AnyClass {
        return CAShapeLayer.self
    }

    open func shapeLayer() -> CAShapeLayer {
        return self.layer as! CAShapeLayer
    }
}
