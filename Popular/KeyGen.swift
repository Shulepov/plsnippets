//
//  KeyGen.swift
//  davinciquiz
//
//  Created by Mihail Shulepov on 30/08/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import Foundation

class RandomGenerator {
    fileprivate var x: UInt64 = 123456789
    fileprivate var y: UInt64 = 362436069
    fileprivate var z: UInt64 = 521288629
    
    private func xorshf96(_ t: UInt64) -> UInt64 {
        var t = t
        x ^= x << 16
        x ^= x >> 5
        x ^= x << 1
        
        t = x
        x = y
        y = z
        z = t ^ x ^ y
        
        return z
    }
    
    func nextRandomNumber(_ seed: UInt64) -> UInt64 {
        return xorshf96(seed)
    }
    
    fileprivate let alphanum = Array("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".characters)
    
    fileprivate func generateChar(_ seed: UInt64) -> Character {
        let num = nextRandomNumber(seed)
        let idx = num % UInt64(alphanum.count)
        return alphanum[Int(idx)]
    }
    
    func randomString(_ seed: UInt64) -> String {
        return randomString(seed, minLen: 15, maxLen: 20)
    }
    
    func randomString(_ seed: UInt64, minLen: Int, maxLen: Int) -> String {
        let len = minLen + Int(nextRandomNumber(seed) % UInt64(maxLen - minLen))
        var ret = ""
        for _ in 1...len {
            ret.append(generateChar(seed))
        }
        return ret
    }
}
