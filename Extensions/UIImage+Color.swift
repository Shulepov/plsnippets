//
//  UIImage+Color.swift
//  davinci
//
//  Created by Mihail Shulepov on 03/12/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit

class RectImageGenerator {
    var fillColor = UIColor.clear
    var borderColor = UIColor.clear
    var borderWidth: CGFloat = 1.0
    var cornerRadius: CGFloat = 0.0
    
    func generate() -> UIImage {
        let maxOffset = max(cornerRadius, borderWidth)
        let rect = CGRect(x: 0.0, y: 0.0, width: 3.0 + 2 * maxOffset, height: 3.0 + 2 * maxOffset)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        let context = UIGraphicsGetCurrentContext()
        
        context?.setFillColor(fillColor.cgColor)
        context?.setStrokeColor(borderColor.cgColor)
        context?.setLineWidth(borderWidth)
        
        let path = CGPath(roundedRect: rect, cornerWidth: cornerRadius, cornerHeight: cornerRadius, transform: nil)
        context?.addPath(path)
        context?.drawPath(using: .fillStroke)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let inset = 1 + maxOffset
        let edgeInsets = UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
        let resizableImage = image?.resizableImage(withCapInsets: edgeInsets, resizingMode: UIImageResizingMode.tile)
        
        return resizableImage!
    }
}

extension UIImage {
    class func imageWithColor(_ color: UIColor) -> UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
}

extension UIButton {
    func setBackgroundColor(_ color: UIColor, forState state: UIControlState) {
        self.setBackgroundImage(UIImage.imageWithColor(color), for: state)
    }
}
