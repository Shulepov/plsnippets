//
//  UIViewController+Top.swift
//  davinci
//
//  Created by Mihail Shulepov on 29/10/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit

extension UIViewController {
    class func windowTopViewController() -> UIViewController! {
        for window in UIApplication.shared.windows {
            if window.windowLevel == UIWindowLevelNormal {
                if let rootVC = window.rootViewController {
                    return rootVC.topViewController()
                }
            }
        }
        return nil
    }
    
    func topViewController() -> UIViewController! {
        if let presentedVC = self.presentedViewController {
            return presentedVC.topViewController()
            
        } else if let tabBarVC = self as? UITabBarController {
            return tabBarVC.selectedViewController?.topViewController()
            
        } else if let navVC = self as? UINavigationController, let visibleVC = navVC.visibleViewController {
            return visibleVC.topViewController()
            
        } else if let childVC = self.childViewControllers.last {
            return childVC.topViewController()
        }
        return self
    }
}
