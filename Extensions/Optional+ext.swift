//
//  Optional+orElse.swift
//  davinciquiz
//
//  Created by Mihail Shulepov on 04/09/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import Foundation

/// Extensions for Optional type
internal extension Optional {
    /// If is some - return self, else return provided value
    /// Use it in case of lightweight alternative value
    func or(_ val: Wrapped) -> Wrapped  {
        if self != nil {
            return self!
        } else {
            return val
        }
    }
    
    /// If is some - return self, else return result of executing closure
    /// Use it instead of simple 'or' in case when creation of alternative value is more expensive
    func orElse(_ closure: () -> Wrapped) -> Wrapped {
        if self != nil {
            return self!
        } else {
            return closure()
        }
    }
    
    /// If is some - return transformer(self!), else return default value
    func mapOr<U>(_ def: U, transformer: (Wrapped) -> U) -> U {
        if self != nil {
            return transformer(self!)
        } else {
            return def
        }
    }
    
    /// If is some - execute provided function and return it's result, else return nil
    /// May be useful for chaining where no default values can be provided
    func andThen<U>(_ closure: (Wrapped) -> U) -> U? {
        if self == nil {
            return nil
        } else {
            return closure(self!)
        }
    }
}
