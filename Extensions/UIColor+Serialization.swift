//
//  UIColor+Int.swift
//  davinciquiz
//
//  Created by Mihail Shulepov on 04/09/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit

extension UIColor {
    class func fromARGB(_ color: UInt32) -> UIColor {
        let r = CGFloat((color & 0x00FF0000) >> 16) / 255
        let g = CGFloat((color & 0x0000FF00) >> 8) / 255
        let b = CGFloat((color & 0x000000FF) >> 0) / 255
        let a = CGFloat((color & 0xFF000000) >> 24) / 255
        return UIColor(red: r, green: g, blue: b, alpha: a)
    }

    func toARGB() -> UInt32 {
        var comps = [CGFloat](repeating: 0.0, count: 4)
        self.getRed(&comps[0], green: &comps[1], blue: &comps[2], alpha: &comps[3])
        let red = (UInt32(comps[0] * 255) << 16) & 0x00FF0000
        let green = (UInt32(comps[1] * 255) << 8) & 0x0000FF00
        let blue = (UInt32(comps[2] * 255) <<  0) & 0x000000FF
        let alpha = (UInt32(comps[3] * 255) << 24) & 0xFF000000
        return alpha | red | green | blue
    }
    
    class func fromHex(_ hex: String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = cString.substring(from: cString.index(after: cString.startIndex))
        }
        
        let countSymbols = cString.utf16.count
        var value: UInt32 = 0
        Scanner(string: cString).scanHexInt32(&value)
        
        if countSymbols >= 6 {
            let hasAlpha = countSymbols == 8
            return UIColor(
                red: CGFloat((value & 0x00FF0000) >> 16) / 255.0,
                green: CGFloat((value & 0x0000FF00) >> 8) / 255.0,
                blue: CGFloat(value & 0x000000FF) / 255.0,
                alpha: hasAlpha ? CGFloat((value & 0xFF000000) >> 24) / 255.0 : CGFloat(1.0)
            )
        } else if countSymbols == 3 || countSymbols == 4 {
            let hasAlpha = countSymbols == 4
            return UIColor(
                red: CGFloat((value & 0x0F00) >> 8) / 15.0,
                green: CGFloat((value & 0x00F0) >> 4) / 15.0,
                blue: CGFloat(value & 0x000F) / 15.0,
                alpha: hasAlpha ? CGFloat((value & 0xF000) >> 12) / 15.0
                                : CGFloat(1.0)
            )
        }
        return UIColor.gray
    }
    
    class func fromString(_ colorStr: String) -> UIColor {
        let cString: String = colorStr.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        let parts = cString.components(separatedBy: ":")
        if parts.count == 1 {
            return fromHex(cString)
        } else {
            let type = parts[0]
            let value = parts[1]
            if type == "PATTERN" || type == "IMG" || type == "IMAGE" {
                if let img = UIImage(named: value) {
                    return UIColor(patternImage: img)
                } else {
                    NSLog("Can't find pattern image: %@", value)
                }
            }
        }
        NSLog("Can't parse color string: '%@'", cString)
        return UIColor.gray
    }
}
