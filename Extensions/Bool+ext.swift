//
//  Bool+ext.swift
//  davinciquiz
//
//  Created by Mihail Shulepov on 04/09/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import Foundation

/// Some convenience extensions for chaining Bool results
internal extension Bool {
    /// Return provided value if self==true else return nil
    func ifTrue<T>(_ val: T) -> T? {
        if self {
            return val
        } else {
            return nil
        }
    }
    
    /// Return provided value if self==false else return nil
    func ifFalse<T>(_ val: T) -> T? {
        if self == false {
            return val
        } else {
            return nil
        }
    }
    
    /// Execute closure and return it's result if self==true else return nil
    func ifTrueThen<T>(_ closure: () -> T) -> T? {
        if self {
            return closure()
        } else {
            return nil
        }
    }
    
    /// Execute closure and return it's result if self==false else return nil
    func ifFalseThen<T>(_ closure: () -> T) -> T? {
        if !self {
            return closure()
        } else {
            return nil
        }
    }
}
