//
//  GCD+Convenience.swift
//  davinci
//
//  Created by Mihail Shulepov on 20/12/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import Foundation

func delay(_ delay: TimeInterval, block: @escaping ()->()) {
    let when = DispatchTime.now() + Double(Int64(Double(NSEC_PER_SEC) * delay)) / Double(NSEC_PER_SEC)
    DispatchQueue.main.asyncAfter(deadline: when, execute: block)
}

func delay(_ delay: TimeInterval, queue: DispatchQueue, block: @escaping ()->()) {
    let when = DispatchTime.now() + Double(Int64(Double(NSEC_PER_SEC) * delay)) / Double(NSEC_PER_SEC)
    queue.asyncAfter(deadline: when, execute: block)
}
