//
//  UIApplication+Directories.swift
//  Mystery
//
//  Created by Mihail Shulepov on 10/02/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

extension UIApplication {
    class var documentsDirectory: String {
        return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    }
    
    class var cachesDirectory: String {
        return NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).first!
    }
    
    class var downloadsDirectory: String {
        return NSSearchPathForDirectoriesInDomains(.downloadsDirectory, .userDomainMask, true).first!
    }
}
