//
//  UIColor+Operations.swift
//  davinci
//
//  Created by Mihail Shulepov on 26/11/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit

extension UIColor {
    
    /**
    Returns a lighter color by the provided percentage
    
    :param: lighting percent percentage
    :returns: lighter UIColor
    */
    func lighterColor(_ percent : Double) -> UIColor {
        return colorWithBrightnessFactor(CGFloat(1 + percent));
    }
    
    /**
    Returns a darker color by the provided percentage
    
    :param: darking percent percentage
    :returns: darker UIColor
    */
    func darkerColor(_ percent : Double) -> UIColor {
        return colorWithBrightnessFactor(CGFloat(1 - percent));
    }
    
    /**
    Return a modified color using the brightness factor provided
    
    :param: factor brightness factor
    :returns: modified color
    */
    func colorWithBrightnessFactor(_ factor: CGFloat) -> UIColor {
        var hue : CGFloat = 0
        var saturation : CGFloat = 0
        var brightness : CGFloat = 0
        var alpha : CGFloat = 0
        
        if getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha) {
            return UIColor(hue: hue, saturation: saturation, brightness: brightness * factor, alpha: alpha)
        } else {
            return self;
        }
    }
    
    var alpha: CGFloat {
        var alpha: CGFloat = 0
        getRed(nil, green: nil, blue: nil, alpha: &alpha)
        return alpha
    }
}
