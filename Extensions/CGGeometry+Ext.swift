//
//  CGGeometry+Ext.swift
//  Football
//
//  Created by Mikhail Shulepov on 20/03/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import Foundation
import CoreGraphics

public func + (lhs: CGPoint, rhs: CGPoint) -> CGPoint {
    return CGPoint(x: lhs.x + rhs.x, y: lhs.y + rhs.y)
}

public func - (lhs: CGPoint, rhs: CGPoint) -> CGPoint {
    return CGPoint(x: lhs.x - rhs.x, y: lhs.y - rhs.y)
}

public func / (lhs: CGPoint, rhs: CGFloat) -> CGPoint {
    return CGPoint(x: lhs.x / rhs, y: lhs.y / rhs)
}

public func * (lhs: CGPoint, rhs: CGFloat) -> CGPoint {
    return CGPoint(x: lhs.x * rhs, y: lhs.y * rhs)
}

public func += (lhs: inout CGPoint, rhs: CGPoint) {
    lhs.x += rhs.x
    lhs.y += rhs.y
}

public extension CGPoint {
    public func distanceTo(_ point: CGPoint) -> CGFloat {
        return sqrt(self.squaredDistanceTo(point))
    }
    
    public func squaredDistanceTo(_ point: CGPoint) -> CGFloat {
        let delta = self - point
        return delta.x * delta.x + delta.y * delta.y
    }
    
    public func normalized() -> CGPoint {
        return (self / sqrt(self.x*self.x + self.y * self.y))
    }   
}
