//
//  UIResponder+FirstResponder.swift
//  davinci
//
//  Created by Mihail Shulepov on 05/10/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit

private var _currentFirstResponder: UIResponder?

extension UIResponder {
    class func currentFirstResponder() -> UIResponder? {
        _currentFirstResponder = nil
        UIApplication.shared.sendAction(#selector(UIResponder.findFirstResponder(_:)), to: nil, from: nil, for: nil)
        return _currentFirstResponder
    }
    
    func findFirstResponder(_ sender: AnyObject!) {
        _currentFirstResponder = self
    }
}
