//
//  UIViewController+ScrollKeyboard.swift
//  davinci
//
//  Created by Mihail Shulepov on 05/10/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func handleKeyboardWithRootScrollView(scrollView: UIScrollView) -> Disposable {
        NSNotificationCenter.defaultCenter()
            .addObserver(self, selector: "keyboardWillAppear:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter()
            .addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification, object: nil)
        
        let nc = NSNotificationCenter.defaultCenter()
        let disposable = CompositeDisposable()
        disposable += nc.rac_notifications(name: UIKeyboardWillShowNotification, object: nil)
            |> start { [unowned self] notification in
                self.keyboardWillAppear(notification.userInfo, scrollView: scrollView)
            }
        
        disposable += nc.rac_notifications(name: UIKeyboardWillHideNotification, object: nil)
            |> start { [unowned self] notification in
                self.keyboardWillHide(notification.userInfo, scrollView: scrollView)
            }
        
        return disposable
    }
    
    private func keyboardWillAppear(info: [NSObject: AnyObject]!, scrollView: UIScrollView) {
        let size = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
        let endSize = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        let height = max(size.height, endSize.height)
        let origin = endSize.minY
        var edgeInsets: UIEdgeInsets?
        if let responder = UIResponder.currentFirstResponder() as? UIView {
            let windowSpaceFrame = responder.convertRect(responder.bounds, toView: nil)
            let offset = windowSpaceFrame.maxY - origin + 25
            if offset > 0 {
                edgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: offset, right: 0)
            }
            
        } else {
            edgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: height + 25, right: 0)
        }
        if let insets = edgeInsets {
            scrollView.contentInset = insets
            scrollView.scrollIndicatorInsets = insets
        }
    }
    
    private func keyboardWillHide(info: [NSObject: AnyObject]!, scrollView: UIScrollView) {
        let duration = (info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        var curve = UIViewAnimationCurve.EaseInOut
        (info[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber).getValue(&curve)
        let contentInsets = UIEdgeInsetsZero
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(duration)
        UIView.setAnimationCurve(curve)
        scrollView.contentInset = contentInsets;
        scrollView.scrollIndicatorInsets = contentInsets;
        UIView.commitAnimations()
    }
}